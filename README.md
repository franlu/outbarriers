# OutBarriers #

# Objetivo #
Crear un proyecto Django con base de datos sqlite3 donde se implemente la ficha de un local perteneciente a los 40 locales esenciales de Outbarriers

# Especificación #

* Dos páginas:
    1. Página índice (ruta "/") incluirá una lista con los N locales que estén disponibles (a añadir desde el panel de administración). Cada elemento de la lista es un enlace a la ficha de dicho local.

    2. Página detalle de local (ruta "/<slug-nombre-de-local>"). En dicha página aparece los datos para ese local.

* Diseño frontend: responsive design (ver zip adjunto) con Bootstrap 3. Font Awesome (opcional)

* Datos de un local:

    - dt: Datetime de cuando se creo la instancia
    - dtMod: Datetime de cuando se modificó por última vez
    - slug: URL friendly para acceso, único
    - name: Texto de hasta 255 caracteres
    - kind: enumerado con 3 tipos (bar, retail y other)
    - description: Texto sin tamaño máximo
    - logo: campo imagen
    - image: campo imagen
    - tags: puede ser un M2M o algún componente 3th party que trabaje con array de strings en los modelos

* Gestión desde el panel de administración
Poder añadir, editar y eliminar locales desde el panel de administracion. La cuenta de superusuario que sea con los parametros de login/password: admin / vamos

# Valoración #
por supuesto se valorará la calidad de código, la inclusion de robots.txt y sitemap.xml, limpieza del código, GIT y en general esos pequeños detalles de cada uno :)

# Material adicional #
mockup de la ficha de local
