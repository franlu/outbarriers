from django.contrib import admin

from .models import Etiqueta, Ficha

admin.site.register(Etiqueta)
admin.site.register(Ficha)
