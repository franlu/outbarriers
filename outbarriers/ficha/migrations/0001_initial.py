# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Etiqueta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=25)),
            ],
            options={
                'ordering': ('name',),
                'db_table': 'etiquetas',
            },
        ),
        migrations.CreateModel(
            name='Ficha',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dt', models.DateTimeField(auto_now_add=True)),
                ('dtMod', models.DateTimeField(auto_now=True)),
                ('slug', models.SlugField(unique=True, max_length=30)),
                ('name', models.CharField(max_length=255)),
                ('kind', models.IntegerField(default=3, choices=[(1, b'bar'), (2, b'retail'), (3, b'other')])),
                ('description', models.TextField()),
                ('logo', models.ImageField(max_length=24576, upload_to=b'logos')),
                ('image', models.ImageField(max_length=24576, upload_to=b'imagenes')),
                ('tags', models.ManyToManyField(to='ficha.Etiqueta', blank=True)),
            ],
            options={
                'ordering': ('name',),
                'db_table': 'fichas',
            },
        ),
    ]
