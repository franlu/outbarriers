# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ficha', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ficha',
            name='image',
            field=models.ImageField(max_length=24576, upload_to=b'ficha/static/imagenes'),
        ),
        migrations.AlterField(
            model_name='ficha',
            name='logo',
            field=models.ImageField(max_length=24576, upload_to=b'ficha/static/logos'),
        ),
    ]
