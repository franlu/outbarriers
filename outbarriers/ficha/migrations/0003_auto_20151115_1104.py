# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ficha', '0002_auto_20151114_2241'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ficha',
            name='image',
            field=models.ImageField(upload_to='ficha/static/ficha/imagenes', max_length=24576),
        ),
        migrations.AlterField(
            model_name='ficha',
            name='kind',
            field=models.IntegerField(choices=[(1, 'bar'), (2, 'retail'), (3, 'other')], default=3),
        ),
        migrations.AlterField(
            model_name='ficha',
            name='logo',
            field=models.ImageField(upload_to='ficha/static/ficha/logos', max_length=24576),
        ),
    ]
