# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ficha', '0003_auto_20151115_1104'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ficha',
            name='image',
            field=models.ImageField(max_length=24576, upload_to='ficha/img'),
        ),
        migrations.AlterField(
            model_name='ficha',
            name='logo',
            field=models.ImageField(max_length=24576, upload_to='ficha/logos'),
        ),
    ]
