# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ficha', '0004_auto_20151118_1100'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ficha',
            name='name',
            field=models.CharField(verbose_name='Nombre', max_length=255),
        ),
    ]
