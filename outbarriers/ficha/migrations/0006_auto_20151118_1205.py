# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ficha', '0005_auto_20151118_1203'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ficha',
            name='description',
            field=models.TextField(verbose_name='Descripcion'),
        ),
        migrations.AlterField(
            model_name='ficha',
            name='image',
            field=models.ImageField(upload_to='ficha/img', verbose_name='Imagen', max_length=24576),
        ),
        migrations.AlterField(
            model_name='ficha',
            name='kind',
            field=models.IntegerField(choices=[(1, 'bar'), (2, 'retail'), (3, 'other')], default=3, verbose_name='Tipo'),
        ),
        migrations.AlterField(
            model_name='ficha',
            name='logo',
            field=models.ImageField(upload_to='ficha/logos', verbose_name='Logo', max_length=24576),
        ),
        migrations.AlterField(
            model_name='ficha',
            name='tags',
            field=models.ManyToManyField(blank=True, verbose_name='Etiquetas', to='ficha.Etiqueta'),
        ),
    ]
