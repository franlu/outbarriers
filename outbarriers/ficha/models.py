from django.db import models
from django.db.models.signals import post_delete
from django.dispatch import receiver

class Etiqueta(models.Model):
    """Modelo para representar etiquetas asociadas a locales"""
    name = models.CharField(max_length=25, unique=True)

    class Meta:
        db_table = 'etiquetas'
        ordering = ('name',)

    def __str__(self):
        return u'%s' % self.name


class Ficha(models.Model):
    """Modelo para representar la ficha de un local."""
    BAR = 1
    RETAIL = 2
    OTHER = 3
    KIND_CHOICES = (
        (BAR, 'bar'),
        (RETAIL, 'retail'),
        (OTHER, 'other'),
    )
    dt = models.DateTimeField(auto_now_add=True)
    dtMod = models.DateTimeField(auto_now=True)
    slug = models.SlugField(max_length=30, unique=True)
    name = models.CharField(max_length=255, verbose_name="Nombre")
    kind = models.IntegerField(choices=KIND_CHOICES, default=OTHER, verbose_name="Tipo")
    description = models.TextField(verbose_name="Descripcion")
    logo = models.ImageField(null=False, upload_to='ficha/logos', max_length=24576, verbose_name="Logo")
    image = models.ImageField(null=False, upload_to='ficha/img', max_length=24576, verbose_name="Imagen")
    tags = models.ManyToManyField(Etiqueta, blank=True, verbose_name="Etiquetas")

    class Meta:
        db_table = 'fichas'
        ordering = ('name',)

    def __str__(self):
        return u'%s' % self.name


@receiver(post_delete, sender=Ficha)
def ficha_delete(sender, instance, **kwargs):
    """Borra los ficheros de las fichas que se eliminan"""
    instance.image.delete(False)
    instance.logo.delete(False)
