from django.shortcuts import render, get_object_or_404
from django.views import generic

from .models import Ficha

class IndexView(generic.ListView):
    template_name = 'ficha/index.html'
    context_object_name = 'fichas'

    def get_queryset(self):
        return Ficha.objects.order_by('name')

class DetailView(generic.DetailView):
    model = Ficha
    template_name = 'ficha/detalle.html'
