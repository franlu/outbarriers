from django.conf.urls import include, url
from django.contrib import admin

from ficha import views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),

    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<slug>[-\w]+)/$', views.DetailView.as_view(), name='detalle'),


]
